 
import React, { createContext, useState } from 'react'


export const AuthContext = createContext(null);
const AuthProvider = ({children}) => {
    const [isLoading, setIsLoading] = useState(true);
    const [logUser, setUser] = useState(null);
    
    
const values={
    isLoading,
    logUser,
    setUser,
  
}
    return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
}

export default AuthProvider