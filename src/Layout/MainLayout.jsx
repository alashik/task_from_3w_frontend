import React from 'react'
import Navigation from '../components/Navbar/Navigation'
import Home from '../components/Home/Home'
import { Outlet } from 'react-router-dom'
import Footer from '../components/Footer/Footer'
 
 

const MainLayout = () => {
  return (
    <div>
        <Navigation />
        <Outlet />
        <Footer />
    </div>
  )
}

export default MainLayout