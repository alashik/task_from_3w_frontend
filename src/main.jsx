import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { RouterProvider } from "react-router-dom";
import router from "./Routes/Routes.jsx";
import { Auth0Provider } from "@auth0/auth0-react";
import AuthProvider from "./Provider/AuthProvider.jsx";
import { Toaster } from "react-hot-toast";

ReactDOM.createRoot(document.getElementById("root")).render(
  <>
  <AuthProvider>
  <Auth0Provider
      domain="dev-chgccjujhm7a1zsv.us.auth0.com"
      clientId="WE0E41AtJjpAs3VRKCyBvIGLK0nafI4S"
      authorizationParams={{
        redirect_uri: window.location.origin,
      }}
    >
      <RouterProvider router={router} />

      <Toaster />
    </Auth0Provider>
  </AuthProvider>
 
  </>
);
