import { createBrowserRouter } from "react-router-dom";
import MainLayout from "../Layout/MainLayout";
import Home from "../components/Home/Home";
import Login from "../components/Login/Login";
import Register from "../components/Register/Register";
import Dashboard from "../components/Dashboard/Dashboard";


const router = createBrowserRouter([
    {
      path: "/",
      element: <MainLayout />,
      children :[
       {
        path :'/',
        element :<Home />
       },
       {
        path :'/login',
        element :< Login />
       },
       {
        path :'/register',
        element :<Register />
       },
       {
        path :'/dashboard',
        element :<Dashboard />
       },
      ]
    },
  ]);

  export default router