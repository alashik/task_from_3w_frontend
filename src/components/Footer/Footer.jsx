import React from 'react'
 import './footer.css'

const Footer = () => {
  return (
    <div>
         <footer className="footer">
 
        <p className="text-center ">
          &copy; Copyright 2022 - All Rights Reserved by Faucet
        </p>
     
    </footer>
    </div>
  )
}

export default Footer