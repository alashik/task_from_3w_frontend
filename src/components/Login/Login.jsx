import React, { useContext, useState } from 'react'
import  '../Register/register.css'
import { FcGoogle } from "react-icons/fc";
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { AuthContext } from '../../Provider/AuthProvider';
import toast from 'react-hot-toast';
import { useAuth0 } from "@auth0/auth0-react";


const Login = () => {
  const { setUser}= useContext(AuthContext)
  const navigate= useNavigate()
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { loginWithRedirect } = useAuth0();
  

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = async(event) => {
    event.preventDefault();
    // Handle signup logic here
    console.log('Email:', email);
    console.log('Password:', password);
    const data = {
      email,
      password
    }
    try {
      const res = await axios.post('https://task-from-backend-3w.vercel.app/login', data)
      console.log(res.data.user);
      setUser(res.data.user)

      if(res.status == 200){
        toast.success('Login Success')
        navigate('/')
      }
    
      console.log(res.data);
    } catch (error) {
      toast.error('Something went wrong')
    }

  };
  return (
    <div className='registerPage'>
        
    <Container>
  <Row className="justify-content-center align-items-center    ">
    <Col md={6}>
    <div className='FormSection m-5  bg-white p-4 '>
      <h1 className='text-center fw-bold mb-4 ' style={{ fontSize: '28px' }}>Login</h1>
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email </Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={handleEmailChange}
            className='mb-3'
        
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={handlePasswordChange}
            className='mb-3'
          />
        </Form.Group>

        <Button type="submit" className='w-100 commonBtn'>
           Login
        </Button>
      </Form>
      <div className="mt-3">
        <p className='text-center fw-bold '>Don't have an account? <Link  to="/register" className='commonColor'>Sign Up</Link></p>
      </div>
      <div className="mt-3  ">
        <p className='text-center'>Or </p>
        <Button className='socialLogin'  onClick={() => loginWithRedirect()}> <FcGoogle /> </Button>
      </div>
    
      </div>
    </Col>
  </Row>
</Container>
  
</div>
  )
}

export default Login