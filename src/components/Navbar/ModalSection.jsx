import React from 'react'
import { Button, Modal, Row } from 'react-bootstrap'
import './modalSection.css'
import img1 from '../../assets/MetaMask_Fox.svg.d9c41a5680a1daaae624.png'
import img2 from '../../assets/WalletConnect.d0b10794.svg'
const ModalSection = (props) => {
  return (
    <div>
              <Modal
        {...props}
       
        aria-labelledby="contained-modal-title-vcenter"
        centered
        className='p-5'
      >
          <Modal.Header closeButton><h4 className='fw-bold '>Connect your wallet</h4></Modal.Header>
       
        <Modal.Body className='modalBody py-5 px-5'>
           
            <Row className='gap-4 ms-auto'>
                <div className='col-5 modalImg  '>
                <img src={img1}  alt="" />
                <p className='waletText'>Meta Musk</p>
                </div>
                <div className='col-5 modalImg '>
                <img src={img2}  alt="" />
                <p className='waletText'>WaletConnect</p>
                </div>
            </Row>
        </Modal.Body>
      
      </Modal>
    </div>
  )
}

export default ModalSection