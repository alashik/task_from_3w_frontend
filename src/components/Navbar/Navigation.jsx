import React, { useContext } from "react";
import { FaRegUserCircle } from "react-icons/fa";
import "./navigation.css";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { AuthContext } from "../../Provider/AuthProvider";
import { Button } from "react-bootstrap";
import { IoIosWallet } from "react-icons/io";
import { FaHillAvalanche } from "react-icons/fa6";
import ModalSection from "./ModalSection";
import { IoIosGitNetwork } from "react-icons/io";
import { MdOutlinePolymer } from "react-icons/md";
import { MdOutlineDeviceThermostat } from "react-icons/md";
import { Link } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";
 
const Navigation = () => {
  const { user, isAuthenticated  ,logout } = useAuth0();
  const [modalShow, setModalShow] = React.useState(false);
  const { logUser } = useContext(AuthContext);
  
  console.log(logUser);
  return (
    <div>
      <Navbar expand="lg" className="navigationHeader">
        <Container>
          <Navbar.Brand >
          <Link to='/'  className="navLogo">  Faucets</Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <div className="d-flex align-items-center    ">
                <NavDropdown title="Ethereum Kovan" className="EthText">
                 
                  
                    <NavDropdown.Item className="EtherText"><span className="me-2 "><MdOutlineDeviceThermostat /></span> Arbitrum Rinkeby </NavDropdown.Item>
                    <NavDropdown.Item className="EtherText"><span className="me-2 "><FaHillAvalanche /></span> Avalanche Fuji   </NavDropdown.Item>
                    <NavDropdown.Item className="EtherText"><span className="me-2 "><MdOutlinePolymer /></span> Polygon Mumbai </NavDropdown.Item>
                    <NavDropdown.Item className="EtherText"><span className="me-2 "> <IoIosGitNetwork /></span> POA Network Sokol </NavDropdown.Item>
                    <NavDropdown.Item className="EtherText"><span className="me-2 "><MdOutlineDeviceThermostat /></span> Ethereum Rinkeby </NavDropdown.Item>
                    
              
 
                </NavDropdown>
             
              </div>
              <div className="d-flex align-items-center ">
                <Button
                  className="waletButton d-flex align-items-center  gap-2"
                  onClick={() => setModalShow(true)}
                >
                  <span>
                    <IoIosWallet />
                  </span>{" "}
                  connect Wallet
                </Button>
                <ModalSection
                  show={modalShow}
                  onHide={() => setModalShow(false)}
                />
              </div>
              <NavDropdown title={<FaRegUserCircle />} className="navIcons">

                {isAuthenticated  && isAuthenticated ? <div>
                  <NavDropdown.Item > {user.email} </NavDropdown.Item> 
                  <NavDropdown.Item onClick={() => logout({ logoutParams: { returnTo: window.location.origin } })}   >  Logout </NavDropdown.Item> 
                  </div> :<div>  <NavDropdown.Item   > <Link to={'/login'}>Login</Link></NavDropdown.Item>
                  <NavDropdown.Item   > <Link to={'/register'}>Register</Link></NavDropdown.Item> 
                  </div> }
               
             
             
             
              {logUser?.role ==='admin' &&    <NavDropdown.Item  > <Link to='/dashboard'>Dashboard</Link></NavDropdown.Item>    } 
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default Navigation;
