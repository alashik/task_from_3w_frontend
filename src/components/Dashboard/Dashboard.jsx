import React, { useEffect, useState } from 'react'
import { Container, Table } from 'react-bootstrap'

const Dashboard = () => {
    const [allUsers, setAllUsers]=useState([])
    useEffect(()=>{
        fetch('https://task-from-backend-3w.vercel.app/usersData')
        .then(res => res.json())
        .then(data => setAllUsers(data))
    }, [])

    console.log(allUsers);
  return (
    <div className='h-100 '>
        <Container>
        <div className='w-50 m-auto mt-5  '>
            <Table bordered>
              <thead>
                <tr className="text-center">
                  <th>#</th>
                  <th>User Id</th>
                  <th>User Email</th>
                  <th>Role</th>
                </tr>
              </thead>
              <tbody>
                
              {allUsers.map((alluser, i) => (
                  <tr className="text-center " key={i}>
                    <td>{i}</td>
                    <td>{alluser._id}</td>
                    <td>{alluser.email}</td>
                   
                    <td>{alluser.role}</td>
                  </tr>
                ))}
               
              </tbody>
            </Table>
          </div>
        </Container>
    </div>
  )
}

export default Dashboard