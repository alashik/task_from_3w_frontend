import React from 'react'
import './bannerSec.css'
import { Container } from 'react-bootstrap'
import WaletSec from '../WaletSection/WaletSec'
const BannerSec = () => {
  return (
    <div className='bannerSecBody'>
      <Container>
      <div className=' bannerText'>
            <h2>Request testnet LINK</h2>
            <p>Get testnet LINK for an account on one of the supported blockchain testnets so you can <br /> create and test your own oracle and Chainlinked smart contract</p>
         </div>

         <WaletSec />
      </Container>
    </div>
  )
}

export default BannerSec