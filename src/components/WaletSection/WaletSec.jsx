import React, { useState } from "react";
import "./waletSection.css";
import Form from "react-bootstrap/Form";
import { IoIosWarning } from "react-icons/io";
import { Button, Row } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import ReCAPTCHA from "react-google-recaptcha";
const WaletSec = () => {
  const reqHistory2 = [
    {
      id: 1,
      time: "12:30 AM",
      amount: 748,
      hash: "s4e7s8er",
    },
    {
      id: 1,
      time: "12:30 AM",
      amount: 974,
      hash: "sfe7r7sr4fer",
    },
    {
      id: 1,
      time: "12:30 AM",
      amount: 874,
      hash: "7s7effkeurusue4",
    },
  ];
  const reqHistory = [
    {
      id: 1,
      time: "12:30 AM",
      amount: 487,
      hash: "4s8er5s5fe57rjmxnfuewrurks",
    },
    {
      id: 2,
      time: "1:30 AM",
      amount: 487,
      hash: "4s8er5s5fe57rjmxnfuewrurks",
    },
    {
      id: 3,
      time: "2:30 AM",
      amount: 487,
      hash: "4s8er5s5fe57rjmxnfuewrurks",
    },
  ];

  const [currentHistory, setCurrentHistory] = useState(reqHistory);
  const [activeButton, setActiveButton] = useState("history1");
  const [verified, setVerified] = useState(false);

  const toggleData = (data, button) => {
    setCurrentHistory(data);
    setActiveButton(button);
  };
  function onChange(value) {
    console.log("Captcha value:", value);
  }
  return (
    <div className="walet">
      <div className="waletWarnning">
        <span className="waletIcon">
          {" "}
          <IoIosWarning />{" "}
        </span>
        <p className="waletText">
          Your wallet is connected to <strong>Ethereum Kovan</strong>, so you
          are requesting <strong>Ethereum Kovan</strong> Link/ETH.
        </p>
      </div>

      <div className="walletAddress  ">
        <Form>
          <Form.Group
            className="mb-3 mt-3"
            controlId="exampleForm.ControlInput1"
          >
            <Form.Label className="EmailTitle">Wallet address</Form.Label>
            <Form.Control type="email" placeholder="Your Wallet Address" />
          </Form.Group>
          <Row>
            <Form.Label className="EmailTitle">Request Type</Form.Label>
            <div className="col-6">
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Control type="email" defaultValue={"20 test Link"} />
              </Form.Group>
            </div>
            <div className="   col-6">
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Control type="email" defaultValue={"0.5 ETH"} />
              </Form.Group>
            </div>
          </Row>
          <Button className="formBtn">Send Request</Button>

          <div className="mt-4">
            <ReCAPTCHA sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI" onChange={onChange} />
          </div>
        </Form>

        <div className="waletHistory">
          <p>Request History</p>
          <div className=" d-flex gap-2 mb-3  mt-2">
            <Button
              onClick={() => toggleData(reqHistory, "history1")}
              variant={activeButton === "history1" ? "primary" : "light"}
              className={activeButton === "history1" ? "active" : ""}
            >
              ETH Transaction History
            </Button>
            <Button
              onClick={() => toggleData(reqHistory2, "history2")}
              variant={activeButton === "history2" ? "primary" : "light"}
              className={activeButton === "history2" ? "active" : ""}
            >
              TestLink Transcation History
            </Button>
          </div>

          <div>
            <Table bordered>
              <thead>
                <tr className="text-center">
                  <th>Sr</th>
                  <th>Time</th>
                  <th>Amount</th>
                  <th>HAsh</th>
                </tr>
              </thead>
              <tbody>
                {currentHistory.map((history) => (
                  <tr className="text-center ">
                    <td>{history.id}</td>
                    <td>{history.time}</td>
                    <td>{history.amount}</td>
                    <td>{history.hash}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WaletSec;
