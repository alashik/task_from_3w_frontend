import React from 'react'
import Navigation from '../Navbar/Navigation'
import Notice from '../Notice/Notice'
import BannerSec from '../Bannersection/BannerSec'

const Home = () => {
  return (
    <div>
        <Notice />
        <BannerSec />
    </div>
  )
}

export default Home